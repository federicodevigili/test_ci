
#include <cmath>
#include <iostream>
using namespace std; 

bool isPrime(int num){
    bool flag=true;
    for(int i = 2; i <= num / 2; i++) {
       if(num % i == 0) {
          flag = false;
          break;
       }
    }
    return flag;
}

int main(){
  
  int count = 100000000;
  while (true) {
    count ++;
    if (isPrime(count))
      cout << "Is Prime: " << to_string(count) << endl;
    
  }
}
